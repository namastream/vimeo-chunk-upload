var VimeoUpload =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 6);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entities_request__ = __webpack_require__(9);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__entities_header__ = __webpack_require__(10);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entities_response__ = __webpack_require__(11);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__utils_utils__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__enums_status_enum__ = __webpack_require__(1);





/**
 * Created by kfaulhaber on 31/03/2017.
 */
class HttpService {
    /**
     * constructor
     * @param maxAcceptedUploadDuration
     */
    constructor(maxAcceptedUploadDuration) {
        this.maxAcceptedUploadDuration = maxAcceptedUploadDuration;
    }
    /**
     * DefaultResolver that decides if the xhr response is valid, and sends custom Response
     * @param xhr
     * @returns {Response}
     * @constructor
     */
    static DefaultResolver(xhr) {
        let data = null;
        try {
            data = JSON.parse(xhr.response);
        }
        catch (e) {
            data = xhr.response;
        }
        let response = new __WEBPACK_IMPORTED_MODULE_2__entities_response__["a" /* Response */](xhr.status, xhr.statusText, data);
        response.responseHeaders = xhr.getAllResponseHeaders().split("\r\n").filter((rawHeader) => {
            return rawHeader.length > 0;
        }).map((rawHeader) => {
            let index = rawHeader.indexOf(":");
            return new __WEBPACK_IMPORTED_MODULE_1__entities_header__["a" /* Header */](rawHeader.slice(0, index).trim(), rawHeader.slice(index + 1).trim());
        });
        if (xhr.status > 308) {
            response.statusCode = __WEBPACK_IMPORTED_MODULE_4__enums_status_enum__["a" /* Status */].Rejected;
        }
        else {
            response.statusCode = __WEBPACK_IMPORTED_MODULE_4__enums_status_enum__["a" /* Status */].Resolved;
        }
        return response;
    }
    /**
     * send method that sets the headers, the different callbacks and sends a request with data.
     * @param request
     * @param statData
     * @returns {Promise<T>}
     */
    send(request, statData = null) {
        return new Promise((resolve, reject) => {
            let xhr = new XMLHttpRequest();
            xhr.open(request.method, request.url, true);
            request.headers.forEach((header) => xhr.setRequestHeader(header.title, header.value));
            xhr.onload = () => {
                if (statData !== null) {
                    statData.end = new Date();
                    statData.done = true;
                }
                let response = HttpService.DefaultResolver(xhr);
                switch (true) {
                    case response.statusCode === __WEBPACK_IMPORTED_MODULE_4__enums_status_enum__["a" /* Status */].Resolved:
                        resolve(response);
                        break;
                    default:
                        reject(response);
                }
            };
            xhr.onabort = () => {
                reject(new __WEBPACK_IMPORTED_MODULE_2__entities_response__["a" /* Response */](xhr.status, xhr.statusText, xhr.response));
            };
            xhr.onerror = () => {
                reject(new __WEBPACK_IMPORTED_MODULE_2__entities_response__["a" /* Response */](xhr.status, xhr.statusText, xhr.response));
            };
            if (statData != null) {
                xhr.upload.addEventListener("progress", (data) => {
                    if (data.lengthComputable) {
                        statData.loaded = data.loaded;
                        statData.total = data.total;
                        statData.end = new Date();
                        //TODO: Symplify this.
                        if (__WEBPACK_IMPORTED_MODULE_3__utils_utils__["a" /* TimeUtil */].TimeToSeconds(statData.end.getTime() - statData.start.getTime()) > statData.prefferedDuration * 2) {
                            statData.loaded = 0;
                            statData.total = 0;
                            statData.done = true;
                            xhr.abort();
                        }
                    }
                });
            }
            try {
                xhr.send(request.data);
            }
            catch (e) {
                console.error("An error occured while sending.", e);
            }
        });
    }
    /**
     * Method that takes raw information to build a Request object.
     * @param method
     * @param url
     * @param data
     * @param headers
     * @returns {Request}
     * @constructor
     */
    static CreateRequest(method, url, data = null, headers = null) {
        let headerList = [];
        for (let prop in headers) {
            if (headers.hasOwnProperty(prop)) {
                headerList.push(new __WEBPACK_IMPORTED_MODULE_1__entities_header__["a" /* Header */](prop, headers[prop]));
            }
        }
        return new __WEBPACK_IMPORTED_MODULE_0__entities_request__["a" /* Request */](method, url, data, headerList);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = HttpService;



/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Status; });
/**
 * Created by kfaulhaber on 26/07/2017.
 *
 * Status enum for setting the status of a custom Response
 *
 */
var Status;
(function (Status) {
    Status[Status["Neutral"] = 0] = "Neutral";
    Status[Status["Rejected"] = 1] = "Rejected";
    Status[Status["Resolved"] = 2] = "Resolved";
})(Status || (Status = {}));


/***/ }),
/* 2 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by kfaulhaber on 17/07/2017.
 */
class TimeUtil {
    /**
     * TimeToSeconds method that converts miliseconds to seconds
     * @param time
     * @returns {number}
     * @constructor
     */
    static TimeToSeconds(time) {
        return time / 1000;
    }
    /**
     * TimeToString method that takes a time and converts it to a string.
     * @param time
     * @returns {string}
     * @constructor
     */
    static TimeToString(time) {
        let date = new Date(null);
        date.setTime(time);
        return date.toISOString().substr(11, 8);
    }
    /**
     * MilisecondsToString method that converts miliseconds to a string time format. HH:MM:SS
     * @param miliseconds
     * @returns {string}
     * @constructor
     */
    static MilisecondsToString(miliseconds) {
        let seconds = TimeUtil.TimeToSeconds(miliseconds);
        let date = new Date(null);
        date.setSeconds(seconds);
        return date.toISOString().substr(11, 8);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = TimeUtil;



/***/ }),
/* 3 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by Kfaulhaber on 30/06/2017.
 *
 * Basic route creater
 *
 */
const VIMEO_ROUTES = {
    DEFAULT: (uri = "") => `https://api.vimeo.com${uri}`,
    TICKET: () => `${VIMEO_ROUTES.DEFAULT()}/me/videos`,
    VIDEOS: (videoId) => `${VIMEO_ROUTES.DEFAULT(`/videos/${videoId}`)}`
};
/* harmony export (immutable) */ __webpack_exports__["a"] = VIMEO_ROUTES;



/***/ }),
/* 4 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by kfaulhaber on 13/07/2017.
 */
const DEFAULT_VALUES = {
    preferredUploadDuration: 20,
    chunkSize: 1024 * 1024,
    token: "TOKEN_STRING_HERE",
    name: "",
    description: "",
    file: null,
    upgrade_to_1080: false,
    timeInterval: 150,
    maxAcceptedFails: 20,
    maxAcceptedUploadDuration: 60,
    useDefaultFileName: false,
    retryTimeout: 5000,
    videoData: {},
    editVideoOnComplete: true,
};
/* harmony export (immutable) */ __webpack_exports__["b"] = DEFAULT_VALUES;

const DEFAULT_EVENTS = {
    chunkprogresschanged: (event) => console.log(`Default: Chunk Progress Update: ${event.detail}/100`),
    totalprogresschanged: (event) => console.log(`Default: Total Progress Update: ${event.detail}/100`),
    //Only for FATAL errors
    vimeouploaderror: () => { },
    vimeouploadcomplete: () => { },
    //All other errors
    vimeouploadwarning: () => { }
};
/* harmony export (immutable) */ __webpack_exports__["a"] = DEFAULT_EVENTS;

const ERRORS = [
    {
        id: "001",
        code: "CHUNK_ERROR",
        title: "Chunk upload failed.",
        detail: "Partial upload failed.",
        type: "minor"
    },
    {
        id: "002",
        code: "CHUNK_ABORT",
        title: "Chunk upload was aborted.",
        detail: "Partial upload exceeded maximum time limit.",
        type: "minor"
    },
    {
        id: "003",
        code: "MAX_FAILURE_LIMIT_ATTAINED",
        title: "Maximum failure limit attained.",
        detail: "Number of accepted minor errors exceeded.",
        type: "fatal"
    },
    {
        id: "004",
        code: "EDIT_PERMISSION_DENIED",
        title: "Edit scope was not provided.",
        detail: "A the [EDIT] scope is required to add a name and description after a video has been uploaded.",
        type: "minor"
    }
];
/* unused harmony export ERRORS */



/***/ }),
/* 5 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__config_config__ = __webpack_require__(4);

/**
 * Created by kfaulhaber on 17/07/2017.
 *
 * EventService class
 *
 * Composed of static methods for handly events
 *
 */
class EventService {
    /**
     * Add static method that registers a valid event name.
     * @param eventName
     * @param callback
     * @constructor
     */
    static Add(eventName, callback) {
        window.addEventListener(eventName, callback, false);
    }
    /**
     * Remove static method that unregisters a listener with a valid event name
     * @param eventName
     * @param callback
     * @constructor
     */
    static Remove(eventName, callback) {
        window.removeEventListener(eventName, callback, false);
    }
    /**
     * Dispatch static method that emits an event
     * @param eventName
     * @param data
     * @constructor
     */
    static Dispatch(eventName, data = null) {
        let customEvent = new CustomEvent(eventName, { detail: data });
        window.dispatchEvent(customEvent);
    }
    /**
     * Exists static method that checks if an event name is valid.
     * @param eventName
     * @returns {boolean}
     * @constructor
     */
    static Exists(eventName) {
        return __WEBPACK_IMPORTED_MODULE_0__config_config__["a" /* DEFAULT_EVENTS */].hasOwnProperty(eventName);
    }
    /**
     * GetDefault static method that returns the default handler for a given event.
     * @param eventName
     * @returns {any}
     * @constructor
     */
    static GetDefault(eventName) {
        return __WEBPACK_IMPORTED_MODULE_0__config_config__["a" /* DEFAULT_EVENTS */][eventName];
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = EventService;



/***/ }),
/* 6 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app__ = __webpack_require__(7);

var module;
/**
 * Created by kfaulhaber on 30/06/2017.
 */
/**
 * Binding library to exports
 * @type {App}
 */
module.exports = __WEBPACK_IMPORTED_MODULE_0__app__["a" /* App */];


/***/ }),
/* 7 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_ticket_ticket_service__ = __webpack_require__(8);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_chunk_chunk_service__ = __webpack_require__(13);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_upload_upload_service__ = __webpack_require__(15);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__config_config__ = __webpack_require__(4);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_media_media_service__ = __webpack_require__(16);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__services_http_http_service__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__services_stat_stat_service__ = __webpack_require__(18);








class App {
    constructor() {
        //Defining other properties
        //Total amount of chunk upload failures
        this.failCount = 0;
    }
    /**
     * Method that initializes the VimeoUpload library. Called everytime an upload is started.
     * Resets all the services and properties. To see "config/config.ts" for all properties that can be added to options.
     * @param options
     */
    init(options = {}) {
        let values = {};
        //We loop through all the default values and see if options overides specific ones. All new properties are added to values object.
        for (let prop in __WEBPACK_IMPORTED_MODULE_3__config_config__["b" /* DEFAULT_VALUES */]) {
            if (__WEBPACK_IMPORTED_MODULE_3__config_config__["b" /* DEFAULT_VALUES */].hasOwnProperty(prop)) {
                values[prop] = (options.hasOwnProperty(prop)) ? options[prop] : __WEBPACK_IMPORTED_MODULE_3__config_config__["b" /* DEFAULT_VALUES */][prop];
            }
        }
        this.maxAcceptedFails = values.maxAcceptedFails;
        this.httpService = new __WEBPACK_IMPORTED_MODULE_6__services_http_http_service__["a" /* HttpService */](values.maxAcceptedUploadDuration);
        this.mediaService = new __WEBPACK_IMPORTED_MODULE_5__services_media_media_service__["a" /* MediaService */](this.httpService, values.file, values.videoData, values.upgrade_to_1080, values.useDefaultFileName, values.editVideoOnComplete);
        this.chunkService = new __WEBPACK_IMPORTED_MODULE_1__services_chunk_chunk_service__["a" /* ChunkService */](this.mediaService, values.preferredUploadDuration, values.chunkSize);
        this.statService = new __WEBPACK_IMPORTED_MODULE_7__services_stat_stat_service__["a" /* StatService */](values.timeInterval, this.chunkService);
        this.ticketService = new __WEBPACK_IMPORTED_MODULE_0__services_ticket_ticket_service__["a" /* TicketService */](values.token, this.httpService, values.upgrade_to_1080);
        this.uploadService = new __WEBPACK_IMPORTED_MODULE_2__services_upload_upload_service__["a" /* UploadService */](this.mediaService, this.ticketService, this.httpService, this.statService);
    }
    /**
     * Start method that'll initiate the upload, create the upload ticket and start the upload loop.
     * @param options
     */
    start(options = {}) {
        this.init(options);
        this.ticketService.open()
            .then((response) => {
            this.ticketService.save(response);
            this.statService.start();
            this.process();
        }).catch((error) => {
            if (this.canContinue()) {
                this.failCount++;
                __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Dispatch("vimeouploaderror", { message: `Error creating ticket.`, error });
                setTimeout(() => {
                    this.start(options);
                }, this.retryTimeout);
            }
        });
    }
    /**
     * Process method that will seek the next chunk to upload
     */
    process() {
        let chunk = this.chunkService.create();
        this.uploadService.send(chunk).then((response) => {
            this.chunkService.updateSize(this.statService.getChunkUploadDuration());
            this.check();
        }).catch(error => {
            if (this.canContinue()) {
                this.failCount++;
                __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Dispatch("vimeouploadwarning", { message: `Unable to send chunk.`, error });
                this.chunkService.updateSize(this.statService.getChunkUploadDuration());
                setTimeout(() => {
                    this.check();
                }, this.retryTimeout);
            }
        });
    }
    /**
     * Check method that is called after each chunk upload to update the byte range.
     */
    check() {
        this.uploadService.getRange().then((response) => {
            switch (response.status) {
                case 308:
                    //noinspection TypeScriptValidateTypes
                    let range = response.responseHeaders.find((header) => {
                        if (header === null && header === undefined)
                            return false;
                        return header.title.toLowerCase() === "range";
                    });
                    this.chunkService.updateOffset(range.value);
                    if (this.chunkService.isDone()) {
                        this.done();
                        return;
                    }
                    this.process();
                    break;
                case 200 || 201:
                    this.done();
                    break;
                default:
                    console.warn(`Unrecognized status code (${response.status}) for chunk range.`);
            }
        }).catch(error => {
            __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Dispatch("vimeouploadwarning", { message: `Unable to get range.`, error });
            if (this.canContinue()) {
                this.failCount++;
                setTimeout(() => {
                    this.check();
                }, this.retryTimeout);
            }
        });
    }
    /**
     * Done method that is called when an upload has been completed. Closes the upload ticket.
     */
    done() {
        this.statService.totalStatData.done = true;
        this.ticketService.close().then((response) => {
            this.statService.stop();
            try {
                //noinspection TypeScriptValidateTypes
                let vimeoId = parseInt(response.responseHeaders.find((header) => {
                    //noinspection TypeScriptValidateTypes
                    if (header === null && header === undefined)
                        return false;
                    return header.title.toLowerCase() === "location";
                }).value.replace("/videos/", ""));
                this.updateVideo(vimeoId);
            }
            catch (error) {
                console.log(`Error retrieving Vimeo Id.`, error);
            }
            console.log(`Delete success:`, response);
        }).catch((error) => {
            this.statService.stop();
            __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Dispatch("vimeouploaderror", { message: `Unable to close upload ticket.`, error });
        });
    }
    /**
     * UpdateVideo method
     * @param vimeoId
     */
    updateVideo(vimeoId) {
        if (this.mediaService.editVideoOnComplete) {
            this.mediaService.updateVideoData(this.ticketService.token, vimeoId).then((response) => {
                let meta = __WEBPACK_IMPORTED_MODULE_5__services_media_media_service__["a" /* MediaService */].GetMeta(vimeoId, response.data);
                __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Dispatch("vimeouploadcomplete", meta);
            }).catch(error => {
                __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Dispatch("vimeouploaderror", {
                    message: `Unable to update video ${vimeoId} with name and description.`,
                    error
                });
                __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Dispatch("vimeouploadcomplete", __WEBPACK_IMPORTED_MODULE_5__services_media_media_service__["a" /* MediaService */].GetMeta(vimeoId));
            });
        }
        else {
            __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Dispatch("vimeouploadcomplete", __WEBPACK_IMPORTED_MODULE_5__services_media_media_service__["a" /* MediaService */].GetMeta(vimeoId));
        }
    }
    /**
     * on method to add a listener. See config/config.ts for a list of available events
     * @param eventName
     * @param callback
     */
    on(eventName, callback = null) {
        if (!__WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Exists(eventName))
            return;
        if (callback === null) {
            callback = __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].GetDefault(eventName);
        }
        __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Add(eventName, callback);
    }
    /**
     * off method to remove a listener. See config/config.ts for a list of available events
     * @param eventName
     * @param callback
     */
    off(eventName, callback = null) {
        if (!__WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Exists(eventName))
            return;
        if (callback === null) {
            callback = __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].GetDefault(eventName);
        }
        __WEBPACK_IMPORTED_MODULE_4__services_event_event_service__["a" /* EventService */].Remove(eventName, callback);
    }
    /**
     * canContinue method checks to see if the amount of failCounts exceed the maxAcceptedFails
     * @returns {boolean}
     */
    canContinue() {
        return (this.maxAcceptedFails === 0) ? true : (this.failCount <= this.maxAcceptedFails);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = App;



/***/ }),
/* 8 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_http_service__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__entities_ticket__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__routes_routes__ = __webpack_require__(3);



/**
 * Created by kfaulhaber on 30/06/2017.
 */
class TicketService {
    /**
     * constructor that takes 3 paramaters
     * @param token
     * @param httpService
     * @param upgrade_to_1080
     */
    constructor(token, httpService, upgrade_to_1080) {
        this.token = token;
        this.httpService = httpService;
        this.upgrade_to_1080 = upgrade_to_1080;
    }
    /**
     * open method that creates a ticket request
     * @returns {Promise<T>}
     */
    open() {
        let data = { type: 'streaming' };
        if (this.upgrade_to_1080) {
            data["upgrade_to_1080"] = this.upgrade_to_1080;
        }
        let request = __WEBPACK_IMPORTED_MODULE_0__http_http_service__["a" /* HttpService */].CreateRequest("POST", __WEBPACK_IMPORTED_MODULE_2__routes_routes__["a" /* VIMEO_ROUTES */].TICKET(), JSON.stringify(data), {
            Authorization: `Bearer ${this.token}`,
            'Content-Type': 'application/json'
        });
        return this.httpService.send(request);
    }
    /**
     * save method that takes a response from creating a ticket upload request and saves it
     * @param response
     */
    save(response) {
        this.ticket = new __WEBPACK_IMPORTED_MODULE_1__entities_ticket__["a" /* Ticket */](response.data.upload_link_secure, response.data.ticket_id, response.data.upload_link, response.data.complete_uri, response.data.user);
    }
    /**
     * close method that sends a DELETE request to the ticket's complete Uri to complete and finalize the upload.
     * Called at the end of the upload when all the bytes have been sent.
     * @returns {Promise<T>}
     */
    close() {
        let request = __WEBPACK_IMPORTED_MODULE_0__http_http_service__["a" /* HttpService */].CreateRequest("DELETE", __WEBPACK_IMPORTED_MODULE_2__routes_routes__["a" /* VIMEO_ROUTES */].DEFAULT(this.ticket.completeUri), null, {
            Authorization: `Bearer ${this.token}`
        });
        return this.httpService.send(request);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = TicketService;



/***/ }),
/* 9 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by kfaulhaber on 17/07/2017.
 *
 * Custom Request Entity
 *
 */
class Request {
    constructor(method, url, data = null, headers = []) {
        this.method = method;
        this.url = url;
        this.data = data;
        this.headers = headers;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Request;



/***/ }),
/* 10 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by kfaulhaber on 24/07/2017.
 *
 * Header Class Entity
 */
class Header {
    constructor(title, value) {
        this.title = title;
        this.value = value;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Header;



/***/ }),
/* 11 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__enums_status_enum__ = __webpack_require__(1);

/**
 * Created by kfaulhaber on 20/07/2017.
 *
 * Custom Response Entity
 *
 */
class Response {
    constructor(status, statusText, data = null) {
        this.status = status;
        this.statusText = statusText;
        this.data = data;
        this.statusCode = __WEBPACK_IMPORTED_MODULE_0__enums_status_enum__["a" /* Status */].Neutral;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Response;



/***/ }),
/* 12 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by kfaulhaber on 13/07/2017.
 *
 * Ticket Entity
 *
 */
class Ticket {
    constructor(uploadLinkSecure, ticketId, uploadLink, completeUri, user) {
        this.uploadLinkSecure = uploadLinkSecure;
        this.ticketId = ticketId;
        this.uploadLink = uploadLink;
        this.completeUri = completeUri;
        this.user = user;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Ticket;



/***/ }),
/* 13 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entities_chunk__ = __webpack_require__(14);

/**
 * Created by kfaulhaber on 30/06/2017.
 */
class ChunkService {
    /**
     * constructor
     * @param mediaService
     * @param preferredUploadDuration
     * @param size
     * @param offset
     */
    constructor(mediaService, preferredUploadDuration, size, offset = 0) {
        this.mediaService = mediaService;
        this.preferredUploadDuration = preferredUploadDuration;
        this.size = size;
        this.offset = offset;
    }
    /**
     * updateSize method that updates the next chunk size based on the uploadDuration compares to the prefferedUploadDuration
     * @param uploadDuration
     */
    updateSize(uploadDuration) {
        this.size = Math.floor((this.size * this.preferredUploadDuration) / uploadDuration * ChunkService.Adjuster);
    }
    /**
     * create method that returns the next chunk to upload from the byte array
     * @returns {Chunk}
     */
    create() {
        let end = Math.min(this.offset + this.size, this.mediaService.media.file.size);
        //TODO: Simplify
        if (end - this.offset !== this.size) {
            this.updateSize(end - this.offset);
        }
        let content = this.mediaService.media.file.slice(this.offset, end);
        return new __WEBPACK_IMPORTED_MODULE_0__entities_chunk__["a" /* Chunk */](content, `bytes ${this.offset}-${(end - 1)}/${this.mediaService.media.file.size}`);
    }
    /**
     * updateOffset method that takes a range and updates the offset.
     * @param range
     */
    updateOffset(range) {
        this.offset = parseInt(range.match(/\d+/g).pop(), 10);
    }
    /**
     * isDone method that checks to see if the offset is or is superior to the file size.
     * @returns {boolean}
     */
    isDone() {
        return this.offset >= this.mediaService.media.file.size;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = ChunkService;

ChunkService.Adjuster = 0.7;


/***/ }),
/* 14 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by kfaulhaber on 13/07/2017.
 *
 * Chunk Entity
 *
 */
class Chunk {
    constructor(content, contentRange) {
        this.content = content;
        this.contentRange = contentRange;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Chunk;



/***/ }),
/* 15 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__http_http_service__ = __webpack_require__(0);

/**
 * Created by kfaulhaber on 30/06/2017.
 */
class UploadService {
    /**
     * constructor that has mulitple dependencies to other services
     * @param mediaService
     * @param ticketService
     * @param httpService
     * @param statService
     */
    constructor(mediaService, ticketService, httpService, statService) {
        this.mediaService = mediaService;
        this.ticketService = ticketService;
        this.httpService = httpService;
        this.statService = statService;
    }
    /**
     * Method that sends a request with the video chunk data
     * @param chunk
     * @returns {Promise<T>}
     */
    send(chunk) {
        let statData = this.statService.create();
        this.statService.save(statData);
        let request = __WEBPACK_IMPORTED_MODULE_0__http_http_service__["a" /* HttpService */].CreateRequest("PUT", this.ticketService.ticket.uploadLinkSecure, chunk.content, {
            'Content-Type': this.mediaService.media.file.type,
            'Content-Range': chunk.contentRange
        });
        console.log(chunk.contentRange);
        return this.httpService.send(request, statData);
    }
    /**
     * getRange method that gets the byte range of the already uploaded video content
     * @returns {Promise<T>}
     */
    getRange() {
        let request = __WEBPACK_IMPORTED_MODULE_0__http_http_service__["a" /* HttpService */].CreateRequest("PUT", this.ticketService.ticket.uploadLinkSecure, null, {
            'Content-Type': this.mediaService.media.file.type,
            'Content-Range': 'bytes */* '
        });
        console.log(request);
        return this.httpService.send(request);
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = UploadService;



/***/ }),
/* 16 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__entities_media__ = __webpack_require__(17);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__http_http_service__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__routes_routes__ = __webpack_require__(3);



/**
 * Created by kfaulhaber on 21/07/2017.
 */
class MediaService {
    /**
     * constructor that initiates the services with the list of dependencies
     * @param httpService
     * @param file
     * @param data
     * @param upgrade_to_1080
     * @param useDefaultFileName
     * @param editVideoOnComplete
     */
    constructor(httpService, file, data, upgrade_to_1080, useDefaultFileName, editVideoOnComplete) {
        this.httpService = httpService;
        this.editVideoOnComplete = editVideoOnComplete;
        if (useDefaultFileName) {
            data["name"] = file.name;
        }
        this.media = new __WEBPACK_IMPORTED_MODULE_0__entities_media__["a" /* Media */](file, data, upgrade_to_1080);
    }
    /**
     * updateVideoData method that sends a request to edit video information.
     * Will not work if the token does not have the "EDIT" scope. Will return a 403 forbidden.
     * @param {string} token
     * @param {number} vimeoId
     * @returns {Promise<T>}
     */
    updateVideoData(token, vimeoId) {
        let params = this.media.data;
        let query = Object.keys(params).map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`).join('&');
        let request = __WEBPACK_IMPORTED_MODULE_1__http_http_service__["a" /* HttpService */].CreateRequest("PATCH", __WEBPACK_IMPORTED_MODULE_2__routes_routes__["a" /* VIMEO_ROUTES */].VIDEOS(vimeoId), query, {
            Authorization: `Bearer ${token}`
        });
        return this.httpService.send(request);
    }
    /**
     * GetMeta static method returns an object with data from updateVideoData response
     * @param {number} vimeoId
     * @param {object} data
     * @returns {{id: number, link: (any|HTMLLinkElement|(function(string): string)), name: any, uri: any, createdTime: any}}
     */
    static GetMeta(vimeoId, data = {}) {
        return {
            id: vimeoId,
            link: data.link || null,
            name: data.name || null,
            uri: data.uri || null,
            createdTime: data.created_time || null
        };
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = MediaService;



/***/ }),
/* 17 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by kfaulhaber on 20/07/2017.
 */
class Media {
    /**
     * constructor
     * @param file
     * @param data
     * @param upgrade_to_1080
     */
    constructor(file, data, upgrade_to_1080) {
        this.file = file;
        this.data = data;
        this.upgrade_to_1080 = upgrade_to_1080;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = Media;



/***/ }),
/* 18 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__event_event_service__ = __webpack_require__(5);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__utils_utils__ = __webpack_require__(2);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__entities_stat_data__ = __webpack_require__(19);



/**
 * Created by Grimbode on 14/07/2017.
 *
 * Service that takes care of checking the total uploaded content and dispatches events to notify all listeners.
 *
 */
class StatService {
    /**
     * constructor that takes two dependencies timerInterval and chunkService
     * @param timeInterval
     * @param chunkService
     */
    constructor(timeInterval, chunkService) {
        this.timeInterval = timeInterval;
        this.chunkService = chunkService;
        this.si = -1;
        this.previousTotalPercent = 0;
    }
    /**
     * start method that starts the interval loop to constantly dispatch events with updated information.
     */
    start() {
        this.totalStatData = this.create(true);
        this.startInterval();
    }
    /**
     * create method creates a new statData with information from  chunkservice.
     * @param isTotal
     * @returns {StatData}
     */
    create(isTotal = false) {
        let date = new Date();
        let size = (isTotal) ? this.chunkService.mediaService.media.file.size : this.chunkService.size;
        return new __WEBPACK_IMPORTED_MODULE_2__entities_stat_data__["a" /* StatData */](date, date, this.chunkService.preferredUploadDuration, 0, size);
    }
    /**
     * save method that takes a statData and saves it to the chunkStatData
     * @param timeData
     */
    save(timeData) {
        this.chunkStatData = timeData;
    }
    /**
     * calculateRatio method that calculates the decimal percent of how much has been uploaded (can be chunk or total)
     * @param loaded
     * @param total
     * @returns {number}
     */
    calculateRatio(loaded, total) {
        return loaded / total;
    }
    /**
     * calculatePercent method calculates the percent that has been uploaded.
     * @param loaded
     * @param total
     * @returns {number}
     */
    calculatePercent(loaded, total) {
        return Math.floor(this.calculateRatio(loaded, total) * 100);
    }
    /**
     * updateTotal method that updates the total percent that has currently been uploaded.
     */
    updateTotal() {
        this.totalStatData.loaded += this.chunkStatData.total;
    }
    /**
     * startInterval method that starts the interval loop to continously dispatch events with updated information on what has been uploaded.
     */
    startInterval() {
        //If a previous interval exists, stop it.
        if (this.si > -1) {
            this.stop();
        }
        //Create the new loop
        this.si = setInterval(() => {
            //Calculate the chup upload percent
            let chunkPercent = this.calculatePercent(this.chunkStatData.loaded, this.chunkStatData.total);
            //If the chunk upload is completed we set the chunkPercent to 100 and reset the chunkStatDatat to 0
            if (this.chunkStatData.done) {
                this.updateTotal();
                this.chunkStatData.total = this.chunkStatData.loaded = 0;
                chunkPercent = 100;
            }
            //Update the total uploaded
            this.totalStatData.loaded = Math.max(this.chunkService.offset, this.totalStatData.loaded);
            //Set the end to the chunk end
            this.totalStatData.end = this.chunkStatData.end;
            //Set the previous total percent value to the new highest
            this.previousTotalPercent = Math.max(this.totalStatData.loaded + this.chunkStatData.loaded, this.previousTotalPercent);
            //Calculate the current total percent
            let totalPercent = this.calculatePercent(this.previousTotalPercent, this.totalStatData.total);
            //emit chunk percent
            __WEBPACK_IMPORTED_MODULE_0__event_event_service__["a" /* EventService */].Dispatch("chunkprogresschanged", chunkPercent);
            if (this.totalStatData.done) {
                totalPercent = 100;
            }
            //emit total percent
            __WEBPACK_IMPORTED_MODULE_0__event_event_service__["a" /* EventService */].Dispatch("totalprogresschanged", totalPercent);
        }, this.timeInterval);
    }
    /**
     * stop method that stops the interval loop, which will stop the flow of dispatched events.
     */
    stop() {
        clearInterval(this.si);
    }
    /**
     * getChunkUploadDuration method that returns the current time it has taken to upload a chunk.
     * @returns {number}
     */
    getChunkUploadDuration() {
        return __WEBPACK_IMPORTED_MODULE_1__utils_utils__["a" /* TimeUtil */].TimeToSeconds(this.chunkStatData.end.getTime() - this.chunkStatData.start.getTime());
    }
    /**
     * chunkIsOverPrefferedUploadTime method that checks if the duration time has exceeded the preffered upload duration.
     * @returns {boolean}
     */
    chunkIsOverPrefferedUploadTime() {
        return this.getChunkUploadDuration() >= this.chunkService.preferredUploadDuration * 1.5;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = StatService;



/***/ }),
/* 19 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/**
 * Created by kfaulhaber on 24/07/2017.
 *
 * StatData Entity
 *
 */
class StatData {
    constructor(start, end, prefferedDuration, loaded = 0, total = 0, done = false) {
        this.start = start;
        this.end = end;
        this.prefferedDuration = prefferedDuration;
        this.loaded = loaded;
        this.total = total;
        this.done = done;
    }
}
/* harmony export (immutable) */ __webpack_exports__["a"] = StatData;



/***/ })
/******/ ]);
//# sourceMappingURL=vimeo-upload.js.map